const chai = require("chai");
const { assert } = require("chai");
const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {
	it("Test API get users is running", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	it("Test API get users returns an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.isArray(res.body);
			done();
		})
	})
	it("Test API get users array first object username is Jojo", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})
	it("Test API get users array last object is not undefined", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.notEqual(res.body[res.body.length - 1], undefined);
			done();
		})
	})
	it("Test API post users returns 400 if no name", (done) => {
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("Test API post user returns 400 if no age", (done) => {
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			name: "Jin",
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
})

describe("API Test Suite for artists", () => {
	it("Test API get artists' response", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	it("Test API get artist - first object's song property is array", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isArray(res.body[0].songs);
			done();
		})
	})
	it("Test API post artist - returns 400 if no name", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			songs: ["World's Smallest Violin" , "Bang!"],
			album: "Neotheater",
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	it("Test API post artist - returns 400 if no songs", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "AJR",
			album: "Neotheater",
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	it("Test API post artist - returns 400 if no album", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "AJR",
			songs: ["World's Smallest Violin" , "Bang!"],
			isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	it("Test API post artist - returns 400 if isActive is false", (done) => {
		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "AJR",
			songs: ["World's Smallest Violin" , "Bang!"],
			album: "Neotheater",
			isActive: false
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
})