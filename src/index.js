const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]


//Controllers
app.get("/users", (req, res) => {
	return res.send(users);
})

app.post("/users", (req, res) => {
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter"
		})
	}
})

//Activity
let artists = [
	{
		name: "Yoasobi",
		songs: ["Tabun" , "Racing Into The Night"],
		album: "Notebook",
		isActive: true
	},
	{
		name: "NF",
		songs: ["When I Grow Up" , "Let You Down"],
		album: "Cloud",
		isActive: true
	},
	{
		name: "Panic! At The Disco",
		songs: ["High Hopes" , "Hallelujah"],
		album: "Pray For The Wicked",
		isActive: true
	}
]

//Controllers / Routes
app.get("/artists", (req, res) => {
	return res.send(artists)
})

app.post("/artists", (req, res) => {
	if(!req.body.hasOwnProperty("name")) return res.status(400).send({error: "Bad Request - missing required parameter NAME"});
	if(!req.body.hasOwnProperty("songs")) return res.status(400).send({error: "Bad Request - missing required parameter SONGS"});
	if(!req.body.hasOwnProperty("album")) return res.status(400).send({error: "Bad Request - missing required parameter ALBUM"});
	if(req.body.isActive === false) return res.status(400).send({error: "Bad Request - isActive is false"});
})

app.listen(port, () => console.log(`API running at localhost:${port}`));